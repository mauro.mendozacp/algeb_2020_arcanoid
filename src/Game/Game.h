#ifndef GAME_H
#define GAME_H

#include "../Entities/Paddle/Paddle.h" 
#include "../Entities/Ball/Ball.h" 
#include "../Scenes/Menu/Menu.h"
#include "../Scenes/Pause/Pause.h"
#include "../Entities/Blocks/Blocks.h" 
#include "../Scenes/Gameplay/Gameplay.h"

namespace game
{
    enum class GAME_STATUS
    {
        MAIN_MENU = 1,
        INGAME,
        TEST,
        PAUSE,
        CREDITS,
        EXIT
    };

    extern const int screenWidth;
    extern const int screenHeight;
    extern GAME_STATUS gameStatus;

    void Game();

    void Init();
    void Update();
    void Draw();
    void DeInit();
}

#endif // !GAME_H