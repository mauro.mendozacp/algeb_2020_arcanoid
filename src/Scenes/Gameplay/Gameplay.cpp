#include "Gameplay.h"
#include "../../Game/Game.h"

namespace game {
	namespace gameplay {
		void init()
		{
			game::paddle::GeneratePaddle();
			game::blocks::setPropertiesBlocks(game::screenWidth);
			game::ball::generateBall();
		}
		void update()
		{
			game::paddle::MovePaddle();
			if (!game::ball::ball.launched)
			{
				game::ball::checkLaunch();
			}
			else
			{
				game::ball::moveBall();
			}
			
			game::ball::checkCollisionBlocks();
			game::ball::checkCollisionPaddle();
			game::ball::checkCollisionWalls();

			if (IsKeyPressed(KEY_P)) {
				pause_menu::Init(true);
				gameStatus = GAME_STATUS::PAUSE;
			}
			if (IsKeyPressed(KEY_R))
			{
				init();
			}
		}
		void draw()
		{
			game::paddle::ShowPaddle();
			game::ball::drawBall();
			game::blocks::drawBlocks();
		}
		void deInit()
		{
		}
	}
	namespace test
	{
		const float g = 9.81f;
		const float hMax = 25;

		void init()
		{
			game::paddle::GeneratePaddle();
			game::ball::generateBall();
		}
		void freeFall()
		{
			ball::ball.speed.y += hMax * g * GetFrameTime();
		}
		void update()
		{
			game::paddle::MovePaddle();
			game::paddle::MovePaddle();
			if (!game::ball::ball.launched)
			{
				game::ball::checkLaunch();
			}
			else
			{
				freeFall();
				game::ball::moveBall();
			}
			game::ball::checkCollisionPaddle();
			game::ball::checkCollisionWalls();

			if (IsKeyPressed(KEY_P)) {
				pause_menu::Init(false);
				gameStatus = GAME_STATUS::PAUSE;
			}
		}
		void draw()
		{
			game::paddle::ShowPaddle();
			game::ball::drawBall();
		}
		void deInit()
		{
		}
		
		
	}
}