#include "Pause.h"
#include "..\..\Game\Game.h"

namespace game
{
	namespace pause_menu
	{
		bool ingame = false;
		int option;
		const Color optionColorSelect = YELLOW;
		const Color optionColorNoSelect = LIGHTGRAY;

		void Init(bool sIngame)
		{
			option = 1;
			ingame = sIngame;
		}
		void Update()
		{
			MoveOption();
			if (AcceptOption())
			{
				switch ((PAUSE_MENU)option)
				{
				case PAUSE_MENU::CONTINUE:
					if (ingame)
					{
						game::gameStatus = GAME_STATUS::INGAME;
					}
					else
					{
						game::gameStatus = GAME_STATUS::TEST;
					}
					break;
				case PAUSE_MENU::BACK_TO_MENU:
					gameplay::deInit();
					menu::Init();
					game::gameStatus = GAME_STATUS::MAIN_MENU;
					break;
				default:
					break;
				}

				DeInit();
			}
		}
		void Draw()
		{
			const char* text = "PAUSE";
			int posX = game::screenWidth / 2;
			int posY = game::screenHeight / 4;
			int font = 35;
			Color color = LIGHTGRAY;

			DrawText(text, (posX - (MeasureText(text, font) / 2)), (posY - (font / 2)), font, color);

			posY = game::screenHeight / 2;
			font = 20;
			int spacing = font + 10;
			DrawText("CONTINUE", (posX - (MeasureText("CONTINUE", font) / 2)), (posY - (font / 2)), font, GetColorOptionMenu((int)PAUSE_MENU::CONTINUE));
			DrawText("BACK TO MENU", (posX - (MeasureText("BACK TO MENU", font) / 2)), ((posY + spacing) - (font / 2)), font, GetColorOptionMenu((int)PAUSE_MENU::BACK_TO_MENU));
		}
		void DeInit()
		{
		}

		bool CheckOption(int auxOption)
		{
			switch ((PAUSE_MENU)auxOption)
			{
			case PAUSE_MENU::CONTINUE:
			case PAUSE_MENU::BACK_TO_MENU:
				return true;
			default:
				return false;
			}
		}

		int GetOptionInput()
		{
			if (IsKeyPressed(KEY_UP))
			{
				return (option - 1);
			}
			if (IsKeyPressed(KEY_DOWN))
			{
				return (option + 1);
			}

			return option;
		}

		void MoveOption()
		{
			if (IsKeyPressed(KEY_UP) || IsKeyPressed(KEY_DOWN))
			{
				int auxOption = GetOptionInput();

				if (CheckOption(auxOption))
				{
					option = auxOption;
				}
			}
		}

		bool AcceptOption()
		{
			if (IsKeyPressed(KEY_ENTER))
			{
				return true;
			}

			return false;
		}

		Color GetColorOptionMenu(int auxOption)
		{
			if (option == auxOption)
			{
				return optionColorSelect;
			}

			return optionColorNoSelect;
		}
	}
}