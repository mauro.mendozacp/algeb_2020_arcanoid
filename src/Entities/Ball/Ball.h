#ifndef BALL_H
#define BALL_H

#include "raylib.h"
#include "../../Scenes/Gameplay/Gameplay.h"
#include "..\Paddle\Paddle.h"
#include "../Blocks/Blocks.h"

namespace game
{
	struct Ball {
		Vector2 position;
		Vector2 speed;
		float angle;
		const int radius = 5;
		Color color;
		bool collided;
		bool launched;
	};

	namespace ball {
		extern Ball ball;
		void generateBall();
		void checkCollisionWalls();
		void checkCollisionPaddle();
		void checkCollisionBlocks();
		
		Vector2 getVelocityForX(float angle, float velocityMax);
		void setAngleForX(float porcX);
		void changeDirection(Vector2 p, Rectangle paddle);
		bool checkCollisionPaddle(Vector2 p);
		Vector2 getPaddlePerimeterPosition(Rectangle paddle);
		void changeVelocityForGravity();

		void moveBall();
		void drawBall();
		void checkLaunch();
		void followPaddle();
	}
}
#endif // !BALL_H