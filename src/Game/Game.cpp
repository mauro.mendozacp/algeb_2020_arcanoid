#include "Game.h"

namespace game
{
    const int screenWidth = 600;
    const int screenHeight = 700;
    GAME_STATUS gameStatus = GAME_STATUS::MAIN_MENU;

	void Game()
	{
        // Initialization
        //--------------------------------------------------------------------------------------
        Init();              // Set our game to run at 60 frames-per-second
        //--------------------------------------------------------------------------------------

        // Main game loop
        while (!WindowShouldClose() && gameStatus != GAME_STATUS::EXIT)    // Detect window close button or ESC key
        {
            // Update
            //----------------------------------------------------------------------------------
            Update();
            // TODO: Update your variables here
            //----------------------------------------------------------------------------------

            // Draw
            //----------------------------------------------------------------------------------
            Draw();
            //----------------------------------------------------------------------------------
        }

        // De-Initialization
        //--------------------------------------------------------------------------------------
        DeInit();       // Close window and OpenGL context
        //--------------------------------------------------------------------------------------
	}

    void Init()
    {
        InitWindow(screenWidth, screenHeight, "ARKANOID");

        SetTargetFPS(60);

        gameStatus = GAME_STATUS::MAIN_MENU;
        menu::Init();
    }

    void Update()
    {
        switch (gameStatus)
        {
        case GAME_STATUS::MAIN_MENU:
            menu::Update();
            break;
        case GAME_STATUS::INGAME:
            gameplay::update();
            break;
        case GAME_STATUS::TEST:
            test::update();
            break;
        case GAME_STATUS::PAUSE:
            pause_menu::Update();
            break;
        case GAME_STATUS::CREDITS:
            break;
        case GAME_STATUS::EXIT:
            break;
        default:
            break;
        }
    }

    void Draw()
    {
        BeginDrawing();

        ClearBackground(BLACK);

        switch (gameStatus)
        {
        case GAME_STATUS::MAIN_MENU:
            menu::Draw();
            break;
        case GAME_STATUS::INGAME:
            gameplay::draw();
            break;
        case GAME_STATUS::TEST:
            test::draw();
            break;
        case GAME_STATUS::PAUSE:
            pause_menu::Draw();
            break;
        case GAME_STATUS::CREDITS:
            break;
        case GAME_STATUS::EXIT:
            break;
        default:
            break;
        }

        EndDrawing();
    }

    void DeInit()
    {
        CloseWindow();
    }
}