#ifndef PADDLE_H
#define PADDLE_H

#include "raylib.h"

namespace game
{
	struct KeyBindings
	{
		int left = KEY_A;
		int right = KEY_D;
	};

	struct ScoreData
	{
		char name[3];
		int score;
	};

	struct Paddle
	{
		Rectangle body;
		Color color;
		KeyBindings controls;
		int speed;
		int maxSpeed;
		short lifes;
		ScoreData playerScore;
	};

	namespace paddle
	{
		extern Paddle paddle;

		void GeneratePaddle();
		void MovePaddle();
		void ShowPaddle();
	}
}

#endif // !PADDLE_H