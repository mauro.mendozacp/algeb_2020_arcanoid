#pragma once
#include "raylib.h"

namespace game
{
	namespace menu
	{
		enum class MAIN_MENU
		{
			PLAY = 1,
			TEST,
			CREDITS,
			EXIT
		};

		void Init();
		void Update();
		void Draw();
		void DeInit();

		bool CheckOptionMainMenu(int auxOption);
		int GetOptionInput();
		void MoveOption();
		bool AcceptOption();
		Color GetColorOptionMenu(int auxOption);
	}
}