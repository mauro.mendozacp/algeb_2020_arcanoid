#include "Blocks.h"

namespace game
{
	namespace blocks {
		const short maxblocks = 50;
		short aux_blocks = 0;
		Blocks blocksLevel[maxblocks];

		void setPropertiesBlocks(const int screenWidth) {
			Color cant_colors_blocks[5]{ WHITE,RED,BLUE,GREEN,YELLOW };
			short cant_filas = 5;
			short aux_width = 60;
			short aux_height = 20;
			int aux_color = 0;
			for (short j = 10; j < aux_height * cant_filas; j += aux_height) {
				for (short i = 10; i < screenWidth; i += aux_width) {
					if (aux_blocks < maxblocks) {
						blocksLevel[aux_blocks].color = cant_colors_blocks[aux_color];
						blocksLevel[aux_blocks].active = true;

						blocksLevel[aux_blocks].properties.x = i;
						blocksLevel[aux_blocks].properties.y = j;

						blocksLevel[aux_blocks].properties.width = aux_width - 20;
						blocksLevel[aux_blocks].properties.height = aux_height - 10;

						aux_blocks++;
					}
				}
				aux_color++;

			}

			aux_blocks = 0;
		}
		void drawBlocks() {

			for (short i = 0; i < maxblocks; i++) {
				if (blocksLevel[i].active)
				DrawRectangleRec(blocksLevel[i].properties, blocksLevel[i].color);
			}
		}
	}
}