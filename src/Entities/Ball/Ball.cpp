#include "Ball.h"
#include "../../Entities/Paddle/Paddle.h"
#include <math.h>

using namespace game;
using namespace gameplay;
using namespace paddle;
using namespace blocks;
namespace game
{
	namespace ball
	{
		Ball ball;
		float ballSpeed = 400;

		void generateBall()
		{
			ball.position.x = paddle::paddle.body.x + paddle::paddle.body.width / 2;
			ball.position.y = paddle::paddle.body.y - paddle::paddle.body.height - ball.radius * 1.2f;
			ball.speed.x = 0;
			ball.speed.y = 0;
			ball.angle = 90;
			ball.color = GRAY;
			ball.collided = false;
			ball.launched = false;
		}
		void checkCollisionWalls()
		{
			if (ball.position.x >= (GetScreenWidth() - ball.radius) || ball.position.x <= ball.radius)
			{
				ball.speed.x *= -1.0f;
			}
			if (ball.position.y <= ball.radius)
			{
				ball.speed.y *= -1.0f;
			}
			if (ball.position.y + ball.radius >= GetScreenHeight())
			{
				ball.speed.x = 0;
				ball.speed.y = 0;
				ball.launched = false;
				ball.position.x = paddle::paddle.body.x + paddle::paddle.body.width / 2;
				ball.position.y = paddle::paddle.body.y - paddle::paddle.body.height - ball.radius * 1.2f;
			}
		}
		void checkCollisionPaddle()
		{
			Vector2 p = getPaddlePerimeterPosition(paddle::paddle.body);
			if (checkCollisionPaddle(p))
			{
				if (p.y == paddle::paddle.body.y)
				{
					if (p.x == paddle::paddle.body.x + (paddle::paddle.body.width / 2))
					{
						ball.speed.x = 0;
					}
					else
					{
						float porcX = 0;

						if (p.x < paddle::paddle.body.x + (paddle::paddle.body.width / 2))
						{
							porcX = 100 - (paddle::paddle.body.x + (paddle::paddle.body.width / 2) - ball.position.x) * 100 / (paddle::paddle.body.width / 2);
							setAngleForX(porcX);
							ball.speed = getVelocityForX(ball.angle, ballSpeed);

							ball.speed.x *= -1;
						}
						else
						{
							porcX = (paddle::paddle.body.x + paddle::paddle.body.width - ball.position.x) * 100 / (paddle::paddle.body.width / 2);
							setAngleForX(porcX);
							ball.speed = getVelocityForX(ball.angle, ballSpeed);
						}
					}

					ball.position.y = paddle::paddle.body.y - ball.radius - 1;
					ball.speed.y *= -1;
				}
				else
				{
					changeDirection(p, paddle::paddle.body);
				}
			}
		}

		void setAngleForX(float porcX)
		{
			float ballAngleMax = 90;
			float ballAngleMin = 20;

			ball.angle = ballAngleMin + ((ballAngleMax - ballAngleMin) * porcX / 100);
		}

		Vector2 getVelocityForX(float angle, float velocityMax)
		{
			Vector2 auxVelocity = { 0, 0 };

			auxVelocity.x = velocityMax * static_cast<float>(cos(static_cast<double>(angle) * PI / 180));
			auxVelocity.y = static_cast<float>(sqrt(pow(static_cast<double>(velocityMax), 2) - pow(static_cast<double>(auxVelocity.x), 2)));

			return auxVelocity;
		}

		void checkCollisionBlocks()
		{
			Vector2 p = Vector2();
			for (short i = 0; i < maxblocks; i++)
			{
				p = getPaddlePerimeterPosition(blocksLevel[i].properties);
				if (checkCollisionPaddle(p)&& (blocksLevel[i].active))
				{
					blocksLevel[i].active = false;
					//indicar que se muere

					changeDirection(p, blocksLevel[i].properties);
				}

			}
		}

		void changeDirection(Vector2 p, Rectangle paddle)
		{
			//Collision Up-Left
			if (p.y == paddle.y && p.x == paddle.x)
			{
				ball.position.y = paddle.y - ball.radius - 1;

				if (ball.speed.x <= 0 && ball.speed.y > 0)
				{
					ball.speed.y *= -1;
				}
				if (ball.speed.x > 0 && ball.speed.y > 0)
				{
					ball.speed.y *= -1;
					ball.speed.x *= -1;
				}
				if (ball.speed.x > 0 && ball.speed.y < 0)
				{
					ball.speed.x *= -1;
				}

				return;
			}

			//Collision Up-Right
			if (p.y == paddle.y && p.x == paddle.x + paddle.width)
			{
				ball.position.y = paddle.y - ball.radius - 1;

				if (ball.speed.x >= 0 && ball.speed.y > 0)
				{
					ball.speed.y *= -1;
				}
				if (ball.speed.x < 0 && ball.speed.y > 0)
				{
					ball.speed.y *= -1;
					ball.speed.x *= -1;
				}
				if (ball.speed.x < 0 && ball.speed.y < 0)
				{
					ball.speed.x *= -1;
				}

				return;
			}

			//Collision Down-Left
			if (p.y == paddle.y + paddle.height && p.x == paddle.x)
			{
				ball.position.y = paddle.y + paddle.height + ball.radius + 1;

				if (ball.speed.x <= 0 && ball.speed.y < 0)
				{
					ball.speed.y *= -1;
				}
				if (ball.speed.x > 0 && ball.speed.y < 0)
				{
					ball.speed.y *= -1;
					ball.speed.x *= -1;
				}
				if (ball.speed.x > 0 && ball.speed.y > 0)
				{
					ball.speed.x *= -1;
				}

				return;
			}

			//Collision Down-Right
			if (p.y == paddle.y + paddle.height && p.x == paddle.x + paddle.width)
			{
				ball.position.y = paddle.y + paddle.height + ball.radius + 1;

				if (ball.speed.x >= 0 && ball.speed.y < 0)
				{
					ball.speed.y *= -1;
				}
				if (ball.speed.x < 0 && ball.speed.y < 0)
				{
					ball.speed.y *= -1;
					ball.speed.x *= -1;
				}
				if (ball.speed.x < 0 && ball.speed.y > 0)
				{
					ball.speed.x *= -1;
				}

				return;
			}

			//Collision Up
			if (p.y == paddle.y)
			{
				ball.position.y = paddle.y - ball.radius - 1;
				ball.speed.y *= -1;

				return;
			}

			//Collision Down
			if (p.y == paddle.y + paddle.height)
			{
				ball.position.y = paddle.y + paddle.height + ball.radius + 1;
				ball.speed.y *= -1;

				return;
			}

			//Collision Left
			if (p.x == paddle.x)
			{
				ball.position.x = paddle.x - ball.radius - 1;
				ball.speed.x *= -1;

				return;
			}

			//Collision Right
			if (p.x == paddle.x + paddle.width)
			{
				ball.position.x = paddle.x + paddle.width + ball.radius + 1;
				ball.speed.x *= -1;

				return;
			}
		}

		bool checkCollisionPaddle(Vector2 p)
		{
			double distance = sqrt((static_cast<double>(ball.position.x) - static_cast<double>(p.x)) * (static_cast<double>(ball.position.x) - static_cast<double>(p.x)) + (static_cast<double>(ball.position.y) - static_cast<double>(p.y)) * (static_cast<double>(ball.position.y) - static_cast<double>(p.y)));
			if (distance < ball.radius)
			{
				return true;
			}

			return false;
		}

		Vector2 getPaddlePerimeterPosition(Rectangle paddle)
		{
			Vector2 p = Vector2();

			p.x = ball.position.x;
			if (p.x < paddle.x)
			{
				p.x = paddle.x;
			}
			else if (p.x > paddle.x + paddle.width)
			{
				p.x = paddle.x + paddle.width;
			}

			p.y = ball.position.y;
			if (p.y < paddle.y)
			{
				p.y = paddle.y;
			}
			else if (p.y > paddle.y + paddle.height)
			{
				p.y = paddle.y + paddle.height;
			}

			return p;
		}

		void moveBall()
		{
			ball.position.x += ball.speed.x * GetFrameTime();
			ball.position.y += ball.speed.y * GetFrameTime();

		}
		void drawBall()
		{
			DrawCircleV(ball.position, ball.radius, ball.color);
		}
		void checkLaunch()
		{
			if (IsKeyPressed(KEY_SPACE))
			{
				ball.launched = true;
				ball.speed.y -= ballSpeed;
			}
			else
			{
				followPaddle();
			}
		}
		void followPaddle()
		{

			ball.position.x = paddle::paddle.body.x + paddle::paddle.body.width / 2;

		}
	}
}