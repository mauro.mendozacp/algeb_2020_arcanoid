#ifndef PAUSE_H
#define PAUSE_H

#include "raylib.h"

namespace game
{
	namespace pause_menu
	{
		enum class PAUSE_MENU
		{
			CONTINUE = 1,
			BACK_TO_MENU
		};

		void Init(bool sIngame);
		void Update();
		void Draw();
		void DeInit();

		bool CheckOption(int auxOption);
		int GetOptionInput();
		void MoveOption();
		bool AcceptOption();
		Color GetColorOptionMenu(int auxOption);
	}
}

#endif // !PAUSE_H
